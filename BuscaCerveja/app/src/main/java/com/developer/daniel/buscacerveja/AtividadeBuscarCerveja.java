package com.developer.daniel.buscacerveja;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class AtividadeBuscarCerveja extends AppCompatActivity {
    ListaCervejas tipos = new ListaCervejas();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atividade_buscar_cerveja);
    }

    public void buscarCerveja(View view) {
        TextView listar = (TextView) findViewById(R.id.listar);
        Spinner lista = (Spinner) findViewById(R.id.color);
        String selected = lista.getSelectedItem().toString();

        List<String> listaDeC = tipos.listaTipos(selected);

        StringBuilder s = new StringBuilder();

        for (String i : listaDeC){
            s.append(i).append("\n");
        }

        listar.setText(s);
    }
}
