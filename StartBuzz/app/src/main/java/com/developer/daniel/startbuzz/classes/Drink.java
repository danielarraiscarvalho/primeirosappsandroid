package com.developer.daniel.startbuzz.classes;

import com.developer.daniel.startbuzz.R;

public class Drink {
    String nome;
    String descricao;
    int idImage;

    public static final Drink[] drinks = {new Drink("Suco", "Suco de limão, bem doce com um leve gosto de azedo", R.drawable.suco),
            new Drink("Café", "Café levemente amargo, saudável com pouco teor de acucar", R.drawable.cafe),
            new Drink("Drink", "Tuti Frut, com uma leve quantidade de alcoo, e um pouco de gasolina", R.drawable.drink)};

    public Drink(String nome, String descricao, int idImage) {
        this.nome = nome;
        this.descricao = descricao;
        this.idImage = idImage;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public int getIdImage() {
        return idImage;
    }

    @Override
    public String toString() {
        return nome;
    }
}
