package com.developer.daniel.cadastrodeclientenovo;

/**
 * Created by danie on 13/04/2017.
 */

public class Cliente {
    private Integer id = 0;
    private String nome = "";
    private String endereco = "";
    private String sexo = "";
    private Integer idade = 0;


    public Cliente(Integer id, String nome, String endereco, String sexo, Integer idade) {

    }
    public Cliente(String nome, String endereco, String sexo, Integer idade) {

    }
    public Cliente() {

    }

    public String getEndereco() {
        return endereco.toString();
    }

    public Integer getId() {
        return id.intValue();
    }

    public Integer getIdade() {
        return idade.intValue();
    }

    public String getNome() {
        return nome.toString();
    }

    public String getSexo() {
        return sexo.toString();
    }

    public void setEndereco(String endereco) {
        if (endereco!=null)
        this.endereco = endereco;
    }

    public void setId(Integer id) {
        if (id!=null)
        this.id = id;
    }

    public void setIdade(Integer idade) {
        if (idade!=null)
        this.idade = idade;
    }

    public void setNome(String nome) {
        if (nome!=null)
        this.nome = nome;
    }

    public void setSexo(String sexo) {
        if (sexo!=null)
        this.sexo = sexo;
    }
}
