package com.developer.daniel.cadastrodeclientenovo;

/**
 * Created by danie on 13/04/2017.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by allanromanato on 5/27/15.
 */
public class CriaBanco extends SQLiteOpenHelper {
     static final String NOME_BANCO = "clientes.db";
     static final String TABELA = "clientes";
     static final String ID = "_id";
     static final String NOME = "nome";
     static final String ENDERECO = "endereco";
     static final String SEXO = "sexo";
     static final String IDADE = "idade";
     static final int VERSAO = 2;

    public CriaBanco(Context context) {
        super(context, NOME_BANCO, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE "+TABELA+"("
                + ID + " integer primary key autoincrement,"
                + NOME + " TEXT, "
                + ENDERECO + " TEXT, "
                + SEXO + " TEXT, "
                + IDADE +" TEXT"
                +")";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABELA);
        onCreate(db);
    }
}