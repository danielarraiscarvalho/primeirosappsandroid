package com.developer.daniel.paint;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class tela extends AppCompatActivity {
    Socket cliente = null;
    DataOutputStream canalSaida = null;
    private RelativeLayout tela;
    TextView texto = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela);

        try {
            cliente = new Socket("172.16.1.104", 3067);
            canalSaida = new DataOutputStream(cliente.getOutputStream());
        } catch (IOException e) {

        }

        tela = (RelativeLayout) findViewById(R.id.tela);

        tela.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent motion) {
                texto = new TextView(getApplicationContext());
                texto.setTextColor(Color.BLUE);
                String msg = "";

                if (motion.getAction() == MotionEvent.ACTION_DOWN) {
                    msg = "pressionou na tela. ";
                } else if (motion.getAction() == MotionEvent.ACTION_MOVE) {
                  msg = String.valueOf(motion.getRawX()).split("\\.")[0] + ";" + String.valueOf(motion.getRawY()).split("\\.")[0];
                    tela.removeAllViews();
                    texto.setText(msg);
                    tela.addView(texto);
                    /*try {
                        if (cliente.isConnected() && cliente!=null) canalSaida.writeUTF("0:0");
                    } catch (IOException e) {
                    }*/
                } else if (motion.getAction() == MotionEvent.ACTION_UP) {
                    msg = "soltou na tela. ";
                }


                return true;
            }
        });
    }
}
