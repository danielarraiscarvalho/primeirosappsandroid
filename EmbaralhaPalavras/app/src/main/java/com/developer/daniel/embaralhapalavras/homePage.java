package com.developer.daniel.embaralhapalavras;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.developer.daniel.embaralhapalavras.mecanica.FabricaMecanica;
import com.developer.daniel.embaralhapalavras.mecanica.Interfaces.MecanicaDoJogo;
import com.developer.daniel.embaralhapalavras.mecanica.MecanicaMaisAcertos;
import com.developer.daniel.embaralhapalavras.mecanica.MecanicaMorteSubita;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class homePage extends AppCompatActivity {
    String[] lista;
    TextView t;
    MecanicaDoJogo m;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

    }

    public ArrayList<String> lista() {
        ArrayList<String> array = new ArrayList<>();
        for (String a : lista) {
            array.add(a);
        }
        return array;
    }

    public void iniciarJogo(View v) {
        lista = getResources().getStringArray(R.array.palavras);
        t = (TextView) findViewById(R.id.home);
        m = FabricaMecanica.retornaMecanica(lista());
        ArrayList<MecanicaDoJogo> lm = new ArrayList<>();
        lm.add(m);
        Intent jogo = new Intent(this, Embaralhador2.class);

        m.setLista(lista());
        jogo.putExtra("mecanica",lm);
        jogo.putStringArrayListExtra("lista", lista());
        startActivity(jogo);
    }
}
