package com.developer.daniel.embaralhapalavras;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.developer.daniel.embaralhapalavras.mecanica.FabricaMecanica;
import com.developer.daniel.embaralhapalavras.mecanica.Interfaces.MecanicaDoJogo;
import com.developer.daniel.embaralhapalavras.mecanica.MecanicaMaisAcertos;

import java.util.ArrayList;

public class Embaralhador2 extends AppCompatActivity {
    String[] lista;
    ArrayList<String> array = new ArrayList<>();
    MecanicaDoJogo m = null;
    EditText campo;
    TextView acertou;
    TextView total;
    String resposta = "";
    Intent jogo;
    float pontos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_embaralhador);
        campo = (EditText) findViewById(R.id.campo);
        acertou = (TextView) findViewById(R.id.resultado);
        total = (TextView) findViewById(R.id.pontos);
        lista = getResources().getStringArray(R.array.palavras);
        alterarFoco();
        Intent i = getIntent();
        ArrayList<MecanicaDoJogo> ml  =(ArrayList<MecanicaDoJogo>) i.getSerializableExtra("mecanica");
        m = FabricaMecanica.retornaMecanica(array);
        m.setLista(array);
        m.novaPalavra();
        total.setText(m.getPalavraEmbalharada());
        /*if (m == null) {
            Palavra();
            nova();
        }*/
    }

    /*public void Palavra() {
        for (String a : lista) {
            array.add(a);
        }
        m = new MecanicaMaisAcertos(array);
    }*/

    public void verificar(View v) {
        resposta = campo.getText().toString();
        if (resposta.equalsIgnoreCase("")) {
            acertou.setTextColor(getResources().getColor(R.color.colorTextErrou));
            acertou.setText(getResources().getString(R.string.responda));
        } else if (m.getPalavra().equalsIgnoreCase(campo.getText().toString())) {
            alterarFoco();
            m.jogar(campo.getText().toString());
            pontos = m.getPontos();
            acertou.setTextColor(getResources().getColor(R.color.colorTextAcertou));
            acertou.setText(getResources().getString(R.string.resultadoP));
            total.setText(String.valueOf(pontos));
            campo.setText("");
            nova();
        } else {
            alterarFoco();
            acertou.setTextColor(getResources().getColor(R.color.colorTextErrou));
            acertou.setText(getResources().getString(R.string.resultadoN));
        }
    }

    public void nova() {
        TextView pEmbaralhada = (TextView) findViewById(R.id.p_embaralhada);
        m.novaPalavra();
        String nova = m.getPalavraEmbalharada();
        pEmbaralhada.setText(nova);
    }

    public void alterarFoco() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(campo.getWindowToken(), 0);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void reiniciar(View v) {
        alterarFoco();
        m.reiniciarJogo();
        total.setText("0.0");
        nova();
        campo.setText("");
        acertou.setTextColor(getResources().getColor(R.color.colorTextNeutra));
        acertou.setText(getResources().getString(R.string.responda));
    }
}
